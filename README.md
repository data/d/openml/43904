# OpenML dataset: law-school-admission-bianry

https://www.openml.org/d/43904

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Law School Admissions (Binarized) 
 Survey among students attending law school in the U.S. in 1991. 
 The dataset was obtained from the R-package fairml. 
 The response variable has been changed to a binary version: Whether ugpa is greater than 3.
 The race1 variables has been binarized to labels white and non-white.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43904) of an [OpenML dataset](https://www.openml.org/d/43904). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43904/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43904/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43904/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

